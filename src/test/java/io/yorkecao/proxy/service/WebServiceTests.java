package io.yorkecao.proxy.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static java.lang.System.out;

/**
 * @author Yorke
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WebServiceTests {

    @Autowired
    private WebService webService;

    @Test
    public void getFilterParamsTest() throws Exception {
        String token = "token";
        String url = "url";
        Map<String, String> params = this.webService.getFilterParams(token, url);
        out.println(params.size());
    }
}
