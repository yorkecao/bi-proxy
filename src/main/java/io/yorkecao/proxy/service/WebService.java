package io.yorkecao.proxy.service;

import java.util.Map;

/**
 * @author Yorke
 */
public interface WebService {
    /**
     * 鉴权并获取数据访问控制信息
     * @param token 用户 token
     * @param reportUrl 报表地址
     * @return 数据访问控制信息
     */
    Map<String, String> getFilterParams(String token, String reportUrl) throws Exception;
}
