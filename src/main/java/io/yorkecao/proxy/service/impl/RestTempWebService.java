package io.yorkecao.proxy.service.impl;

import io.yorkecao.proxy.config.ProxyConfig;
import io.yorkecao.proxy.service.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yorke
 */
@Service
public class RestTempWebService implements WebService {
    private static Logger log = LoggerFactory.getLogger(RestTempWebService.class);
    private final ProxyConfig proxyConfig;
    private final RestTemplate restTemplate;

    @Autowired
    public RestTempWebService(ProxyConfig proxyConfig, RestTemplateBuilder restTemplateBuilder) {
        this.proxyConfig = proxyConfig;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Map<String, String> getFilterParams(String token, String reportUrl) throws Exception {
        Map<String, Object> uriVariables = buileUriVariables(token, reportUrl);
        String webServiceInterface = this.proxyConfig.getWebServiceInterface();
        String requestUrl = buildRequestUrl(webServiceInterface);
        ParameterizedTypeReference<Map<String, String>> responseType = new ParameterizedTypeReference<Map<String, String>>() {};
        ResponseEntity<Map<String, String>> responseEntity = this.restTemplate.exchange(requestUrl, HttpMethod.GET, null, responseType, uriVariables);
        log.info(responseEntity.toString());

        if(!isResponseSuccessful(responseEntity)) {
            throw new Exception(responseEntity.toString());
        }

        return  responseEntity.getBody();
    }


    private boolean isResponseSuccessful(ResponseEntity responseEntity) {
        int code = responseEntity.getStatusCodeValue();
        return code >= 200 && code < 300;
    }

    private String buildRequestUrl(String url) {
        return url + "?token={token}&reportUrl={reportUrl}";
    }

    private Map<String, Object> buileUriVariables(String token, String reportUrl) {
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("token", token);
        uriVariables.put("reportUrl", reportUrl);
        return uriVariables;
    }
}
