package io.yorkecao.proxy;

import io.yorkecao.proxy.filter.BiPreFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BiProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiProxyApplication.class, args);
	}

	@Bean
	public BiPreFilter biPreFilter() {
	    return new BiPreFilter();
    }
}
