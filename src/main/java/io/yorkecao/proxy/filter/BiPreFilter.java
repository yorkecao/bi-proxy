package io.yorkecao.proxy.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import io.yorkecao.proxy.config.ProxyConfig;
import io.yorkecao.proxy.service.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.*;

/**
 * @author Yorke
 */
public class BiPreFilter extends ZuulFilter {
    private static Logger log = LoggerFactory.getLogger(BiPreFilter.class);

    private static final String ERROR_INFO = "Authentication failed";
    private static final String ERROR_TOKEN = "get token failed";

    @Autowired
    private WebService webService;
    @Autowired
    private ProxyConfig proxyConfig;

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        return !ctx.containsKey(FORWARD_TO_KEY) // a filter has already forwarded
                && !ctx.containsKey(SERVICE_ID_KEY) // a filter has already determined serviceId
                && ctx.getRequest().getServletPath().contains(this.proxyConfig.getBiPrefix()); // 只处理指定的请求
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        Map<String, List<String>> requestQueryParams = ctx.getRequestQueryParams();
        log.info(request.getMethod() + ":" + request.getRequestURL().toString());

        String token = getToken(request);

        if (token.equals("")) {
            sendErrorResponse(ctx, ERROR_TOKEN);
        } else {
            String servletPath = request.getServletPath();

            try {
                Map<String, String> params = this.webService.getFilterParams(token, servletPath);
                for (String key : params.keySet()) {
                    List<String> paramValues = new ArrayList<>();
                    paramValues.add(params.get(key));
                    requestQueryParams.put(key, paramValues);
                }
                ctx.setRequestQueryParams(requestQueryParams);
            } catch (Exception e) {
                e.printStackTrace();
                sendErrorResponse(ctx, e.getMessage());
            }
        }

        return null;
    }

    /**
     * 从 cookie 中获取 token
     */
    private String getToken(HttpServletRequest request) {
        String token = "";
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(this.proxyConfig.getTokenCookie())) {
                token = cookie.getValue();
            }
        }

        return token;
    }

    /**
     * 鉴权失败处理
     */
    private void sendErrorResponse(RequestContext ctx, String errorMessage) {
        String message = errorMessage != null ? errorMessage : ERROR_INFO;
        log.error(message);
        ctx.setSendZuulResponse(false);
        HttpServletResponse response = ctx.getResponse();
        try {
            String errorPageUrl = this.proxyConfig.getErrorPageUrl();
            if (errorPageUrl != null && !errorPageUrl.equals("")) {
                response.sendRedirect(this.proxyConfig.getErrorPageUrl());
            } else {
                response.setCharacterEncoding(StandardCharsets.UTF_8.name());
                response.setHeader(HttpHeaders.ACCEPT_CHARSET, MediaType.APPLICATION_JSON_UTF8_VALUE);
                response.getWriter().write(message);
            }
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
