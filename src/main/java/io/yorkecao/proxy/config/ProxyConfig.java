package io.yorkecao.proxy.config;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Yorke
 */
@Configuration
@ConfigurationProperties(prefix = "proxy")
public class ProxyConfig {
    @NotBlank private String webServiceInterface;
    @NotBlank private String tokenCookie;
    private String errorPageUrl;
    @NotBlank private String biServerUrl;
    @NotBlank private String biPrefix;

    public String getWebServiceInterface() {
        return webServiceInterface;
    }

    public void setWebServiceInterface(String webServiceInterface) {
        this.webServiceInterface = webServiceInterface;
    }

    public String getTokenCookie() {
        return tokenCookie;
    }

    public void setTokenCookie(String tokenCookie) {
        this.tokenCookie = tokenCookie;
    }

    public String getErrorPageUrl() {
        return errorPageUrl;
    }

    public void setErrorPageUrl(String errorPageUrl) {
        this.errorPageUrl = errorPageUrl;
    }

    public String getBiServerUrl() {
        return biServerUrl;
    }

    public void setBiServerUrl(String biServerUrl) {
        this.biServerUrl = biServerUrl;
    }

    public String getBiPrefix() {
        return biPrefix;
    }

    public void setBiPrefix(String biPrefix) {
        this.biPrefix = biPrefix;
    }
}
