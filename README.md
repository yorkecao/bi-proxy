# 通过 Zuul 代理控制报表访问权限

在将 BI 报表集成到我们自己的门户网站时，可能需要根据登录门户用户的不同，让报表展示不同的数据。

很多 BI 报表工具支持通过 URL 参数为报表过滤器赋值，从而控制报表内容。

对于这类 BI 工具，可以使用该项目提供的思路进行控制。

这样做的动机可能是：

- 不想在门户和报表工具中维护两套用户信息
- 购买的 License 数量不足以和门户用户一一对应




## 主要技术

Spring Cloud Zuul。

Zuul 是 Netflix 开源出来的一个框架，提供动态路由、监控、弹性、安全等边缘服务。

Zuul 也被吸纳为 Spring Cloud 的一部分，可以与其他 Spring Cloud 组件轻松结合，携手解决微服务架构中的治理问题。

在本项目中，我们使用 Zuul 作报表工具的反向代理服务器，并在代理层执行权限控制逻辑。



## 项目架构

如图：

![](/src/main/resources/picture/proxy.png)

- 报表工具所在的域不对外开放，只对 Zuul 反向代理服务器开放；用户要访问报表，必须经过代理器。
- 代理器接收到报表请求后，先调用门户提供的权限控制接口，鉴权成功后拼上返回的过滤器参数，再进行转发动作。
- 需将门户和代理配置到相同的域下，这样登录门户后相关的 Cookie 可以带到访问报表的请求中。这样门户就可以从 Cookie 中获取 token 信息了。
- 权限控制接口负责自定义的权限管控逻辑，例如：先根据 token 获取用户信息；根据用户信息判断是否有报表访问权限；如果有报表访问权限则获取其需要设置的报表过滤器键值对（通过过滤器控制报表显示的内容）。




## 配置项

在 `resources/` 目录下的 `application.yml` 文件中，配置 `proxy` 条目：

- `proxy.web-service-interface`：权限控制接口

  由门户提供的 RESTful 接口，用于权限控制。

- `proxy.token-cookie`：Cookie 中的 token key

  代理通过该 key 从 Cookie 中获取 token。

- `proxy.error-page-url`：错误页面地址

  可选。当鉴权失败时重定向到的地址。没有配置时则显示文字提示。

- `proxy.bi-server-url`：BI 服务器

  报表服务器地址。

- `proxy.bi-prefix`：BI 报表前缀

  报表地址的前缀。代理只会对这些链接做过滤。




## 权限控制接口

```java
Map<String, String> getFilterParams(@RequestParam String token, @RequestParam String reportUrl);
```

传入参数：

- token：用户 token
- reportUrl：要访问的报表 URL

返回值：

- 过滤器 URL 参数的键值对

